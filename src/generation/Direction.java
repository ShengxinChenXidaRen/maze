package generation;

/**
 * Describes all possible directions from the point of view of the robot,
 * i.e., relative to its current forward position.
 * Mind the difference between the robot's point of view
 * and cardinal directions in terms of north,south,east,west.
 */
public enum Direction {
	LEFT(3), RIGHT(1), FORWARD(0), BACKWARD(2);
	private final int value;
	Direction(int nClockwiseRotationsNeeded) {
		this.value = nClockwiseRotationsNeeded % 4;
	}
	
	public int getValue() {
		return value;
	}
	
	public CardinalDirection addTo(CardinalDirection other) {
		CardinalDirection ret = other;
		int rotationsRemaining = (4 - this.value) % 4; // stupid direction inversion
		assert (0 <= rotationsRemaining && rotationsRemaining < 4) : "Illegal value.";
		while(rotationsRemaining > 0) {
			ret = ret.rotateClockwise();
			rotationsRemaining --;
		}
		return ret;
	}

	static Direction fromClockwiseRotations(int nRotations) {
		nRotations = nRotations % 4;
		switch(nRotations) {
		case 0:
			return FORWARD;
		case 1:
			return RIGHT;
		case 2:
			return BACKWARD;
		case 3:
			return LEFT;
		default:
			throw new RuntimeException("Inconsistent enum type.");
		}
	}
}