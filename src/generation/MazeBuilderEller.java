package generation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MazeBuilderEller extends MazeBuilder implements Runnable{
	private int currentPathId = 1;
	
	public MazeBuilderEller() {
		super();
		System.out.println("MazeBuilderEller uses Eller's algorithm to generate maze.");
	}
	

	@Override
	protected void generatePathways() {
		int[] nextRow = new int[width];
		for (int y = 0; y < height; y ++) {
			int[] curRow = assignNewPathId(nextRow, y);
			curRow = breakRightWalls(curRow, y);
			if(y != height - 1) {
				nextRow = breakDownWalls(curRow, y); // this advances to the next row
			}
		}
	}
	
	// 1st tier helper methods
	
	protected int[] breakRightWalls(int[] curRow, int y) {
		for (int x = 0; x < width - 1; x ++) {
			Wall rightWall = new Wall(x, y, CardinalDirection.East);
			// must connect components on last row
			boolean mustBreak = (y == height - 1 && curRow[x] != curRow[x+1]); 
			boolean wantBreak = (curRow[x] != curRow[x+1] && cells.canGo(rightWall));
			if ((mustBreak || wantBreak && random.nextBoolean())) {
				cells.deleteWall(rightWall);
				int setIdToUpdate = curRow[x+1];
				for (int i = x+1; i < width; i++) {
					if (curRow[i] == setIdToUpdate) curRow[i] = curRow[x];
				}
			}
		}
		return curRow;
	}
	
	protected int[] breakDownWalls(int[] curRow, int y) {
		int[] nextRow = new int[width];
		
		Map<Integer, List<Integer>> sets = new HashMap<Integer, List<Integer>>();
		
		for (int i = 0; i < width; i++) {
			if (sets.get(curRow[i]) == null) {
				sets.put(curRow[i], new ArrayList<Integer>());
			}
			sets.get(curRow[i]).add(i);
		}
		
		for (int x = 0; x < width; x ++) {
			Wall downWall = new Wall(x, y, CardinalDirection.South);
			if (cells.hasNoWall(x, y, CardinalDirection.South)) {
				nextRow[x] = curRow[x];
			}
			else if (!order.isPerfect() && cells.canGo(downWall) && random.nextBoolean()) {
				cells.deleteWall(downWall);
				nextRow[x] = curRow[x];
				sets.remove(curRow[x]);
			}
		}
		
		for (List<Integer> entry: sets.values()) {
			int index = random.nextIntWithinInterval(0, entry.size()-1);
			int x = entry.get(index);
			Wall downWall = new Wall(x, y, CardinalDirection.South);
			cells.deleteWall(downWall);
			nextRow[x] = curRow[x];
		}
		
		return nextRow;
	}
	
	// 2nd tier helper methods
	
	private int[] assignNewPathId(int[] curRow, int y) {
		for (int x = 0; x < width; x ++) {
			if (curRow[x] == 0){
				curRow[x] = ++currentPathId;
			}
			else if (x>1 && cells.hasNoWall(x, y, CardinalDirection.West)) {
				curRow[x] = curRow[x-1];
				}
			}
		return curRow;
	}

}
