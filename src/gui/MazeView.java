package gui;

import gui.Constants.StateGUI;
import gui.MazePanel.Color;
import gui.MazePanel.Font;

/**
 * Implements the screens that are displayed whenever the game is not in 
 * the playing state. The screens shown are the title screen, 
 * the generating screen with the progress bar during maze generation,
 * and the final screen when the game finishes.
 * 
 * @author Peter Kemper
 *
 */
public class MazeView {
	private String overMsg;
	
    String[] driverOptions = {"Wizard", "WallFollower", "Pledge", "Explorer"};
    String[] generationOptions = {"DFS", "Eller", "Prim"};
    int[] levelOptions = {0, 1, 2};

	private StateGenerating controllerState; // used for generating screen
    
    public MazeView(StateGenerating c) {
        super() ;
        controllerState = c ;
    }
    /**
     * Draws the title screen, screen content is hard coded
     * @param panel holds the graphics for the off-screen image
     * @param filename is a string put on display for the file
     * that contains the maze, can be null
     */
    void redrawTitle(MazePanel gc, String filename) {
        // produce white background
        gc.setColor(new Color("white"));
        gc.fillRect(0, 0, Constants.VIEW_WIDTH, Constants.VIEW_HEIGHT);
        // write the title 
        gc.setFont(largeBannerFont);
        gc.setColor(new Color("red"));
        gc.centerString("MAZE", 100);
        // write the reference to Paul Falstad
        gc.setColor(new Color("blue"));
        gc.setFont(smallBannerFont);
        gc.centerString("by Paul Falstad", 160);
        gc.centerString("www.falstad.com", 190);
        gc.centerString("press `?` for help", 220);
        // write the instructions
        gc.setColor(new Color("black"));
//        if (filename == null) {
//        	// default instructions
//        	gc.centerString("To start, select a skill level.", 250);
//        	gc.centerString("(Press a number from 0 to 9,", 300);
//        	gc.centerString("or a letter from A to F)", 320);
//        }
//        else {
//        	// message if maze is loaded from file
//        	gc.centerString("Loading maze from file:", 250);
//        	gc.centerString(filename, 300);
//        }
        gc.centerString("Version 4.0", 350);
    }

	
	public void redraw(MazePanel gc, StateGUI state, int px, int py, int view_dx,
			int view_dy, int walk_step, int view_offset, RangeSet rset, int ang) {
		//dbg("redraw") ;
		switch (state) {
		case STATE_TITLE:
			redrawTitle(gc,null);
			break;
		case STATE_GENERATING:
			redrawGenerating(gc);
			break;
		case STATE_PLAY:
			// skip this one
			break;
		case STATE_FINISH:
			redrawFinish(gc, overMsg);
			break;
		}
	}
	
	private void dbg(String str) {
		System.out.println("MazeView:" + str);
	}

	/**
	 * Helper method for redraw to draw final screen, screen is hard coded
	 * @param gc graphics is the off-screen image
	 */
	void redrawFinish(MazePanel gc, String msg) {
		overMsg = msg;
		// produce blue background
		gc.setColor(new Color("blue"));
		gc.fillRect(0, 0, Constants.VIEW_WIDTH, Constants.VIEW_HEIGHT);
		// write the title 
		gc.setFont(largeBannerFont);
		gc.setColor(new Color("yellow"));
		gc.centerString("Game over!", 100);
		// write some extra blurb
		gc.setColor(new Color("orange"));
		gc.setFont(smallBannerFont);
		String[] msgs = msg.split("\n");
		if (msgs.length == 3) {
			gc.centerString(msgs[2], 160);
			gc.centerString(msgs[0], 190);
			gc.centerString(msgs[1], 210);
		}
		else {
			gc.centerString(msg, 160);
		}

		// write the instructions
		gc.setColor(new Color("white"));
		gc.centerString("Hit any key to restart", 300);
	}
	/**
	 * Helper method for redraw to draw screen during phase of maze generation, screen is hard coded
	 * only attribute percentdone is dynamic
	 * @param gc graphics is the off screen image
	 */
	void redrawGenerating(MazePanel gc) {
		// produce yellow background
		gc.setColor(new Color("yellow"));
		gc.fillRect(0, 0, Constants.VIEW_WIDTH, Constants.VIEW_HEIGHT);
		// write the title 
		gc.setFont(largeBannerFont);
		gc.setColor(new Color("red"));
		gc.centerString("Building maze", 150);
		gc.setFont(smallBannerFont);
		// show progress
		gc.setColor(new Color("black"));
		if (null != controllerState) 
		    gc.centerString(controllerState.getPercentDone()+"% completed", 200);
		else
			gc.centerString("Error: no controller, no progress", 200);
		// write the instructions
		gc.centerString("Hit escape to stop", 300);
	}

	final MazePanel.Font largeBannerFont = new Font("TimesRoman", Font.BOLD, 48);
	final Font smallBannerFont = new Font("TimesRoman", Font.BOLD, 16);

}
