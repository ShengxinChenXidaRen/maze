package gui;

import java.util.Random;

import generation.CardinalDirection;
import generation.Direction;
import gui.Robot.Turn;

public class Pledge extends Driver {
	int r = 0;
	
	// initialize turn counter as 0
	int countturn = 0;
	
	void followWall() throws Exception {
		if (robot.hasStopped()) throw new Exception("The robot has stopped.");
		// if there's no wall on the left
		// the robot should go left
		if(robot.distanceToObstacle(Direction.LEFT) != 0) {
			if (r != 4) {
				robot.rotate(Turn.LEFT);
				countturn --;
				r ++;
				robot.move(1, false);
				return;
			}
			else {
				robot.rotate(Turn.AROUND);
				int distToWall = robot.distanceToObstacle(Direction.LEFT);
				robot.rotate(Turn.LEFT);
				countturn ++;
				r = 0;
				robot.move(distToWall, false);
				return;
			}
		} 
		// if both the left and front are blocked
		// adjust the direction by turning right
		else while (robot.distanceToObstacle(Direction.FORWARD)==0) {
			robot.rotate(Turn.RIGHT);
			countturn ++;
		}
		// move forward in current direction
		r = 0;
		robot.move(1, false);
		return;
	}
	
	@Override
	/**
	 * Drives the robot towards the exit given it exists and 
	 * given the robot's energy supply lasts long enough. 
	 * @return true if driver successfully reaches the exit, false otherwise
	 * @throws exception if robot stopped due to some problem, e.g. lack of energy
	 */
	public boolean drive2Exit() throws Exception {
		// pick a direction
		Random random = new Random();
		CardinalDirection [] directions = CardinalDirection.values();
		CardinalDirection picked = directions[random.nextInt(directions.length)];

		// while not at the exit
		while (!robot.isAtExit()) {
			CardinalDirection cur = robot.getCurrentDirection();
			Direction relativeDirection = cur.difference(picked);
			int mainDirDist = robot.distanceToObstacle(relativeDirection);
			
			// move in main direction 
			// when the robot is able to and the turn counter is 0
			if (mainDirDist != 0 && countturn == 0) {
				turn(relativeDirection);
				robot.move(mainDirDist, false);
				// get ready to follow the wall again
				robot.rotate(Turn.RIGHT);
			}
			// otherwise continue following the wall
			else followWall();
		}
		return true;
	}
	
}
