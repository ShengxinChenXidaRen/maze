package gui;

import java.awt.event.KeyEvent;

/**
 * This class contains all constants that are used in the maze package 
 * and shared among several classes.
 * 
 * @author Peter Kemper
 *
 */
public class Constants {


	// The panel used to display the maze has a fixed dimension
	public static final int VIEW_WIDTH = 400;
	public static final int VIEW_HEIGHT = 400;
	public static final int MAP_UNIT = 128;
	public static final int VIEW_OFFSET = MAP_UNIT/8;
	public static final int STEP_SIZE = MAP_UNIT/4;
	// Skill-level 
	// The user picks a skill level between 0 - 9, a-f 
	// The following arrays transform this into corresponding dimensions (x,y) for the resulting maze as well as the number of rooms and parts
	public static int[] SKILL_X =     { 4, 12, 15, 20, 25, 25, 35, 35, 40, 60, 70, 80, 90, 110, 150, 300 };
	public static int[] SKILL_Y =     { 4, 12, 15, 15, 20, 25, 25, 35, 40, 60, 70, 75, 75,  90, 120, 240 };
	public static int[] SKILL_ROOMS = { 0,  2,  2,  3,  4,  5, 10, 10, 20, 45, 45, 50, 50,  60,  80, 160 };
	public static int[] SKILL_PARTCT = { 60, 600, 900, 1200, 2100, 2700, 3300,
	5000, 6000, 13500, 19800, 25000, 29000, 45000, 85000, 85000*4 };
	
	// Possible states of the GUI
	// these are states of an automaton that the graphical user interface implements
	public enum StateGUI { STATE_TITLE, STATE_GENERATING, STATE_PLAY, STATE_FINISH; }
	
	// Possible user input  
	public enum UserInput {
		ReturnToTitle,
		Start,
		Up,
		Down,
		Left,
		Right,
		Jump,
		ToggleLocalMap,
		ToggleFullMap,
		ToggleSolution,
		ZoomIn,
		ZoomOut,
		Drive,
		Help;
		
		/**
		 * For the UserInput ui, tell the user what to press to trigger it,
		 * and what it does
		 * 
		 * @param ui
		 * @return String describing ui.
		 */
	
		public String getHelpString() {
			switch (this) {
			case Drive:
				return "In algorithmic mode, press enter to drive to the end of the maze.";
			default:
				return "";
			}
		}
		
		static public UserInput parseKeyEvent(KeyEvent arg0) {
			int key = arg0.getKeyChar();
			int code = arg0.getKeyCode();
			
			//Possible operations for UserInput based on enum
			// {ReturnToTitle, Start, 
			// Up, Down, Left, Right, Jump, 
			// ToggleLocalMap, ToggleFullMap, ToggleSolution, 
			// ZoomIn, ZoomOut };
			UserInput uikey = null;
			int value = 0;
				
			// translate keyboard input into operation for MazeController
			switch (key) {
			case ('w' & 0x1f): // Ctrl-w makes a step forward even through a wall
				uikey = UserInput.Jump;
				break;
			case '\t': case 'm': // show local information: current position and visible walls
				// precondition for showMaze and showSolution to be effective
				// acts as a toggle switch
				uikey = UserInput.ToggleLocalMap;
				break;
			case 'z': // show a map of the whole maze
				// acts as a toggle switch
				uikey = UserInput.ToggleFullMap;
				break;
			case 's': // show the solution on the map as a yellow line towards the exit
				// acts as a toggle switch
				uikey = UserInput.ToggleSolution;
				break;
			case '+': case '=': // zoom into map
				uikey = UserInput.ZoomIn;
				break ;
			case '-': // zoom out of map
				uikey = UserInput.ZoomOut;
				break ;
			case KeyEvent.VK_ESCAPE: // is 27
				uikey = UserInput.ReturnToTitle;
				break;
			case 'h': // turn left
				uikey = UserInput.Left;
				break;
			case 'j': // move backward
				uikey = UserInput.Down;
				break;
			case 'k': // move forward
				uikey = UserInput.Up;
				break;
			case 'l': // turn right
				uikey = UserInput.Right;
				break;
			case '\n': //drive robot
				uikey = UserInput.Drive;
				break;
			case '?':
				uikey = UserInput.Help;
				break;
			case KeyEvent.CHAR_UNDEFINED: // fall back if key is undefined but code is
				// char input for 0-9, a-f skill-level
				if ((KeyEvent.VK_0 <= code && code <= KeyEvent.VK_9) || (KeyEvent.VK_A <= code && code <= KeyEvent.VK_Z)){
					if (code >= '0' && code <= '9') {
						value = code - '0';
						uikey = UserInput.Start;
					}
					if (code >= 'a' && code <= 'f') {
						value = code - 'a' + 10;
						uikey = UserInput.Start;
					}
				} else {
					if (KeyEvent.VK_ESCAPE == code)
						uikey = UserInput.ReturnToTitle;
					if (KeyEvent.VK_UP == code)
						uikey = UserInput.Up;
					if (KeyEvent.VK_DOWN == code)
						uikey = UserInput.Down;
					if (KeyEvent.VK_LEFT == code)
						uikey = UserInput.Left;
					if (KeyEvent.VK_RIGHT == code)
						uikey = UserInput.Right;
				}
				break;
			default:
				// check ranges of values as possible selections for skill level
				if (key >= '0' && key <= '9') {
					value = key - '0';
					uikey = UserInput.Start;
				} else
				if (key >= 'a' && key <= 'f') {
					value = key - 'a' + 10;
					uikey = UserInput.Start;
				} else
					System.out.println("Constants.UserInput.parseKeyEvent:Error: cannot match input key:" + key);
				break;
			}
			// don't let bad input proceed
			if (null == uikey) {
				System.out.println("Constants.UserInput.parseKeyEvent: ignoring unmatched keyboard input: key=" + key + " code=" + code);
				return null;
			}
			
			assert (0 <= value && value <= 15);		
			// feed user input into controller
			// value is only used in combination with uikey == Start
			return uikey;
		}
	};

	// fixing a value matching the escape key
	final static int ESCAPE = 27;
	
	

}
