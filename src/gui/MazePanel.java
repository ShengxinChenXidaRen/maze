package gui;

import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Panel;
import java.awt.RenderingHints;
import java.lang.reflect.Field;

/**
 * Add functionality for double buffering to an AWT Panel class.
 * Used for drawing a maze.
 * 
 * @author Peter Kemper
 *
 */
public class MazePanel extends Panel  {
	/* Panel operates a double buffer see
	 * http://www.codeproject.com/Articles/2136/Double-buffer-in-standard-Java-AWT
	 * for details
	 */
	// bufferImage can only be initialized if the container is displayable,
	// uses a delayed initialization and relies on client class to call initBufferImage()
	// before first use
	private Image bufferImage;  
	private Graphics2D graphics; // obtained from bufferImage, 
	// graphics is stored to allow clients to draw on the same graphics object repeatedly
	// has benefits if color settings should be remembered for subsequent drawing operations
	
	public static class Color{
		private final java.awt.Color trueColor;
		
		public Color(String name) {
			try {
				Field field = java.awt.Color.class.getField(name);
				trueColor = (java.awt.Color) field.get(null);
			}
			catch (Exception e) {
				throw new RuntimeException("Invalid color name: "+ name);
			}
		}
		
		public Color(int r, int g, int b) {
			trueColor = new java.awt.Color(r, g, b);
		}
		
		public Color(int rgb) {
			trueColor = new java.awt.Color(rgb);
		}
		
		public int getRGB() {
			return trueColor.getRGB();
		}
		
		public java.awt.Color getAWTColor(){
			return trueColor;
		}
	}
	
	
	public static class Font{
		final String name;
		
		
		/*
		 * styles; can be combined with or
		 */
		final int style;
	    public static final int PLAIN       = 0;
	    public static final int BOLD        = 1;
	    public static final int ITALIC      = 2;
	    
		final int size;
	    public Font(String name, int style, int size) {
	        this.name = (name != null) ? name : "Default";
	        this.style = (style & ~0x03) == 0 ? style : 0;
	        this.size = size;
	    }
	    
	    public java.awt.Font toAWTFont(){
	    	return new java.awt.Font(name, style, size);
	    }
	}
	
	
	/**
	 * Constructor. Object is not focusable.
	 */
	public MazePanel() {
		setFocusable(false);
		bufferImage = null; // bufferImage initialized separately and later
		graphics = null;	// same for graphics
	}
	
	@Override
	public void update(Graphics g) {
		paint(g);
	}
	/**
	 * Method to draw the buffer image on a graphics object that is
	 * obtained from the superclass. 
	 * Warning: do not override getGraphics() or drawing might fail. 
	 */
	public void update() {
		paint(getGraphics());
	}
	
	/**
	 * Draws the buffer image to the given graphics object.
	 * This method is called when this panel should redraw itself.
	 * The given graphics object is the one that actually shows 
	 * on the screen.
	 */
	@Override
	public void paint(Graphics g) {
		if (null == g) {
			// System.out.println("MazePanel.paint: no graphics object, skipping drawImage operation");
		}
		else {
			g.drawImage(bufferImage,0,0,null);	
		}
	}

	/**
	 * Obtains a graphics object that can be used for drawing.
	 * This MazePanel object internally stores the graphics object 
	 * and will return the same graphics object over multiple method calls. 
	 * The graphics object acts like a notepad where all clients draw 
	 * on to store their contribution to the overall image that is to be
	 * delivered later.
	 * To make the drawing visible on screen, one needs to trigger 
	 * a call of the paint method, which happens 
	 * when calling the update method. 
	 * @return graphics object to draw on, null if impossible to obtain image
	 */
	public Graphics getBufferGraphics() {
		// if necessary instantiate and store a graphics object for later use
		if (null == graphics) { 
			if (null == bufferImage) {
				bufferImage = createImage(Constants.VIEW_WIDTH, Constants.VIEW_HEIGHT);
				if (null == bufferImage)
				{
					System.out.println("Error: creation of buffered image failed, presumedly container not displayable");
					return null; // still no buffer image, give up
				}		
			}
			graphics = (Graphics2D) bufferImage.getGraphics();
			if (null == graphics) {
				System.out.println("Error: creation of graphics for buffered image failed, presumedly container not displayable");
			}
			else {
				// System.out.println("MazePanel: Using Rendering Hint");
				// For drawing in FirstPersonDrawer, setting rendering hint
				// became necessary when lines of polygons 
				// that were not horizontal or vertical looked ragged
				graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
						RenderingHints.VALUE_ANTIALIAS_ON);
				graphics.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
						RenderingHints.VALUE_INTERPOLATION_BILINEAR);
			}
		}
		return graphics;
	}
	
	public Graphics2D getBufferGraphics2D() {
		return (Graphics2D) getBufferGraphics();
	}
	
	public void setColor(Color c) {
		getBufferGraphics2D().setColor(c.getAWTColor());
	}
	
	public void fillRect(int x, int y, int width, int height) {
		getBufferGraphics2D().fillRect(x,y,width,height);
	}
	
	public void fillPolygon(int[] xPoints,
            int[] yPoints,
            int nPoints) {
		getBufferGraphics2D().fillPolygon(xPoints, yPoints, nPoints);
		
	}
	
	public void setFont(MazePanel.Font font) {
		this.graphics.setFont(font.toAWTFont());
	}
	
	
	public void centerString(String str, int ypos) {
		Graphics2D gc = getBufferGraphics2D(); 
		FontMetrics fm = gc.getFontMetrics();
		gc.drawString(str, (Constants.VIEW_WIDTH-fm.stringWidth(str))/2, ypos);
	}
	
	public void drawLine(int x1, int y1, int x2, int y2) {
		Graphics2D gc = getBufferGraphics2D();
		gc.drawLine(x1, y1, x2, y2);
	}
	
	public void fillOval(int a, int b, int c, int d) {
		Graphics2D gc = getBufferGraphics2D();
		gc.fillOval(a,b,c,d);
	}
	

}
