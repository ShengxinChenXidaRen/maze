/**
 * 
 */
package gui;

import generation.CardinalDirection;
import generation.Direction;
import generation.MazeConfiguration;

/**
 * @author xren
 *
 */
public class BasicRobot implements Robot {
	private final boolean HAS_ROOM_SENSOR = true;
	private final float ENERGY_SENSOR = 1;
	private final float ENERGY_FULL_ROTATION = 12;
	private final float ENERGY_STEP_FORWARD = 5;

	private int odometer = 0;
	private float energy = 3000;
	private boolean stopped = false;
	
	// assigned by BasicRobot.setMaze()
	private Controller controller;
	private StatePlaying sp;
	private MazeConfiguration maze;
	
	/**
	 *  Constructor
	 */
	public BasicRobot() {
		
	}
	
	/**
	 * Turn robot on the spot for amount of degrees. 
	 * If robot runs out of energy, it stops, 
	 * which can be checked by hasStopped() == true and by checking the battery level. 
	 * @param direction to turn and relative to current forward direction. 
	 */
	public void rotate(Turn turn) {
		// check energy
		switch(turn) {
		case AROUND:
			if (energy >= ENERGY_FULL_ROTATION / 2) {
				energy -= ENERGY_FULL_ROTATION / 2;
				sp.rotate(1);
				sp.rotate(1);
			} else {
				System.out.println("Not enough energy for rotating AROUND");
				stopped = true;
			}
			break;
		case LEFT:
			if(energy >= ENERGY_FULL_ROTATION / 4) {
				sp.rotate(1);
				energy -= ENERGY_FULL_ROTATION / 4;
			} else {
				System.out.println("Not enough energy for rotating LEFT");
				stopped = true;
			}
			break;
		case RIGHT:
			if(energy >= ENERGY_FULL_ROTATION / 4) {
				sp.rotate(-1);
				energy -= ENERGY_FULL_ROTATION / 4;
			} else {
				System.out.println("Not enough energy for rotating RIGHT");
				stopped = true;
			}
			break;
		default:
			assert false : "Illegal turn direction specified in BasicRobot.rotate()";
		}
	}
	/**
	 * Moves robot forward a given number of steps. A step matches a single cell.
	 * If the robot runs out of energy somewhere on its way, it stops, 
	 * which can be checked by hasStopped() == true and by checking the battery level. 
	 * If the robot hits an obstacle like a wall, it depends on the mode of operation
	 * what happens. If an algorithm drives the robot, it remains at the position in front 
	 * of the obstacle and also hasStopped() == true as this is not supposed to happen.
	 * This is also helpful to recognize if the robot implementation and the actual maze
	 * do not share a consistent view on where walls are and where not.
	 * If a user manually operates the robot, this behavior is inconvenient for a user,
	 * such that in case of a manual operation the robot remains at the position in front
	 * of the obstacle but hasStopped() == false and the game can continue.
	 * @param distance is the number of cells to move in the robot's current forward direction 
	 * @param manual is true if robot is operated manually by user, false otherwise
	 * @precondition distance >= 0
	 */
	public void move(int distance, boolean manual) {
		// make sure distance satisfies precondition
		assert distance >= 0;
		// step forward distance steps
		for(int i = distance; i > 0; i--) {
			// check energy cost
			if(energy < ENERGY_STEP_FORWARD) {
				stopped = true;
				break;
			}
			// check wall
			if(sp.checkMove(1)) {
				// no wall; deduct energy & walk
				energy -= ENERGY_STEP_FORWARD;
				odometer++;
				sp.walk(1);
			} else {
				// ran into wall; stop according to whether manual or algorithmic
				stopped = !manual;
				
			}
		}
	}
	/**
	 * Provides the current position as (x,y) coordinates for the maze cell as an array of length 2 with [x,y].
	 * @postcondition 0 <= x < width, 0 <= y < height of the maze. 
	 * @return array of length 2, x = array[0], y=array[1]
	 * @throws Exception if position is outside of the maze
	 */
	public int[] getCurrentPosition() throws Exception  {
		// get current position
		int[] pos = controller.getCurrentPosition();
		if(!controller.getMazeConfiguration().getMazecells().inRange(pos)) {
			throw new Exception("Attempting to get position when out of range.");
		}
		return pos;
	}
	/**
	 * Provides the robot with a reference to the controller to cooperate with.
	 * The robot memorizes the controller such that this method is most likely called only once
	 * and for initialization purposes. The controller serves as the main source of information
	 * for the robot about the current position, the presence of walls, the reaching of an exit.
	 * The controller is assumed to be in the playing state.
	 * @param controller is the communication partner for robot
	 * @precondition controller != null, controller is in playing state and has a maze
	 */
	public void setMaze(Controller controller) {
		assert controller.getMazeConfiguration() != null;
		this.controller = controller;
		this.sp = (StatePlaying)controller.currentState;
		this.maze = controller.getMazeConfiguration();
		assert this.controller != null;
		assert this.sp != null;
		System.out.println("SP:" + sp);
		assert this.maze != null;
	}
	/**
	 * Tells if current position (x,y) is right at the exit but still inside the maze. 
	 * Used to recognize termination of a search.
	 * @return true if robot is at the exit, false otherwise
	 */
	public boolean isAtExit() {
		int[] pos = controller.getCurrentPosition();
		assert maze.getMazecells().inRange(pos);
		return maze.getMazecells().isExitPosition(pos[0], pos[1]);
	}
	/**
	 * Tells if a sensor can identify the exit in given direction relative to 
	 * the robot's current forward direction from the current position.
	 * @return true if the exit of the maze is visible in a straight line of sight
	 * @throws UnsupportedOperationException if robot has no sensor in this direction
	 */
	public boolean canSeeExit(Direction direction) throws UnsupportedOperationException {
		// compute sight direction
		CardinalDirection sightDirection = direction.addTo(sp.getCurrentDirection());
		// get current position
		int[] pos = sp.getCurrentPosition();
		while (maze.getMazecells().inRange(pos)){
			if(maze.getMazecells().hasWall(pos[0], pos[1], sightDirection)) {
				return false;
			}
			pos[0] += sightDirection.getDirection()[0];
			pos[1] += sightDirection.getDirection()[1];
		}
		energy -= ENERGY_SENSOR;
		return true;
	}
	/**
	 * Tells if current position is inside a room. 
	 * @return true if robot is inside a room, false otherwise
	 * @throws UnsupportedOperationException if not supported by robot
	 */	
	public boolean isInsideRoom() throws UnsupportedOperationException {
		if(!hasRoomSensor()) {
			throw new UnsupportedOperationException("This robot does not have a room sensor.");
		}
		int[] pos = controller.getCurrentPosition();
		return maze.getMazecells().isInRoom(pos[0], pos[1]);
		
	}
	/**
	 * Tells if the robot has a room sensor.
	 * @return true if robot does have room sensor
	 */
	public boolean hasRoomSensor() {
		return HAS_ROOM_SENSOR;
	}
	/**
	 * Provides the current cardinal direction.
	 * 
	 * @return cardinal direction of robot's current direction in absolute terms
	 */	
	public CardinalDirection getCurrentDirection() {
		return sp.getCurrentDirection();
	}
	/**
	 * Returns the current battery level.
	 * The robot has a given battery level (energy level) 
	 * that it draws energy from during operations. 
	 * The particular energy consumption is device dependent such that a call 
	 * for distance2Obstacle may use less energy than a move forward operation.
	 * If battery level <= 0 then robot stops to function and hasStopped() is true.
	 * @return current battery level, level is > 0 if operational. 
	 */
	public float getBatteryLevel() {
		return energy;
	}
	/**
	 * Sets the current battery level.
	 * The robot has a given battery level (energy level) 
	 * that it draws energy from during operations. 
	 * The particular energy consumption is device dependent such that a call 
	 * for distance2Obstacle may use less energy than a move forward operation.
	 * If battery level <= 0 then robot stops to function and hasStopped() is true.
	 * @param level is the current battery level
	 * @precondition level >= 0 
	 */
	public void setBatteryLevel(float level) {
		energy = level;
	}
	/** 
	 * Gets the distance traveled by the robot.
	 * The robot has an odometer that calculates the distance the robot has moved.
	 * Whenever the robot moves forward, the distance 
	 * that it moves is added to the odometer counter.
	 * The odometer reading gives the path length if its setting is 0 at the start of the game.
	 * The counter can be reset to 0 with resetOdomoter().
	 * @return the distance traveled measured in single-cell steps forward
	 */
	public int getOdometerReading() {
		return odometer;
	}
	/** 
     * Resets the odomoter counter to zero.
     * The robot has an odometer that calculates the distance the robot has moved.
     * Whenever the robot moves forward, the distance 
     * that it moves is added to the odometer counter.
     * The odometer reading gives the path length if its setting is 0 at the start of the game.
     */
	public void resetOdometer() {
		odometer = 0;
	}
	/**
	 * Gives the energy consumption for a full 360 degree rotation.
	 * Scaling by other degrees approximates the corresponding consumption. 
	 * @return energy for a full rotation
	 */
	public float getEnergyForFullRotation() {
		return ENERGY_FULL_ROTATION;
	}
	/**
	 * Gives the energy consumption for moving forward for a distance of 1 step.
	 * For simplicity, we assume that this equals the energy necessary 
	 * to move 1 step backwards and that scaling by a larger number of moves is 
	 * approximately the corresponding multiple.
	 * @return energy for a single step forward
	 */
	public float getEnergyForStepForward() {
		return ENERGY_STEP_FORWARD;
	}
	/**
	 * Tells if the robot has stopped for reasons like lack of energy, hitting an obstacle, etc.
	 * @return true if the robot has stopped, false otherwise
	 */
	public boolean hasStopped() {
		return stopped;
	}
	/**
	 * Tells the distance to an obstacle (a wall or border) 
	 * in a direction as given and relative to the robot's current forward direction.
	 * Distance is measured in the number of cells towards that obstacle, 
	 * e.g. 0 if current cell has a wall in this direction, 
	 * 1 if it is one step forward before directly facing a wall,
	 * Integer.MaxValue if one looks through the exit into eternity.
	 * @return number of steps towards obstacle if obstacle is visible 
	 * in a straight line of sight, Integer.MAX_VALUE otherwise
	 * @throws UnsupportedOperationException if not supported by robot
	 */
	public int distanceToObstacle(Direction direction) throws UnsupportedOperationException {
		if (!hasDistanceSensor(direction)) {
			throw new UnsupportedOperationException("No sensor to tell distanceToObstacle.");
		}
		energy -= ENERGY_SENSOR;
		CardinalDirection moveDirection = direction.addTo(getCurrentDirection());
		int dist = 0;
		int[] pos = sp.getCurrentPosition();
		while(maze.getMazecells().inRange(pos)) {
			if(maze.getMazecells().hasWall(pos[0], pos[1], moveDirection)) {
				return dist;
			}
			dist++;
			pos[0] += moveDirection.getDirection()[0];
			pos[1] += moveDirection.getDirection()[1];
		}
		return Integer.MAX_VALUE;
	}
	/**
	 * Tells if the robot has a distance sensor for the given direction.
	 * Since this interface is generic and may be implemented with robots 
	 * that are more or less equipped. The purpose is to allow for a flexible
	 * robot driver to adapt its driving strategy according the features it
	 * finds supported by a robot.
	 * 
	 * @return whether the robot has a distance sensor in the given direction
	 */

	public boolean hasDistanceSensor(Direction direction) {
		return true;
	}

	
}
