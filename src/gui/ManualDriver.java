/**
 * 
 */
package gui;

import gui.Constants.UserInput;
import gui.Robot.Turn;

/**
 * @author xren
 *
 */
public class ManualDriver extends Driver {	
	/**
	 * Handles user input and drives accordingly.
	 */
	public void handleKey(UserInput key) {
		switch(key) {
        case Up: // move forward
        	this.robot.move(1, true);
        	break;
        case Left: // turn left
        	this.robot.rotate(Turn.LEFT);
        	break;
        case Right:
        	this.robot.rotate(Turn.RIGHT);
        	break;
        case Down:
        	this.robot.rotate(Turn.AROUND);
        	break;
		default:
			assert false : "ManualDriver should only be handling direction keys.";
			break;
		}
	}
}
