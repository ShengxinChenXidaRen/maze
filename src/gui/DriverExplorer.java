package gui;

import static java.lang.Math.max;
import static java.lang.Math.min;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

import generation.CardinalDirection;
import generation.Direction;
import gui.Robot.Turn;

/**
 * Class: 
 * Explorer
 * 
 * Responsibility:
 * Solve the maze based on a random search algorithm
 * 
 * Collaborator:
 * BasicRobot
 *
 * @author xren
 *
 */

public class DriverExplorer extends Driver {
	public final DriverType driverType = DriverType.EXPLORER;
	public static final boolean DEV_MODE = true;
	int[][] cellVisits;
	ArrayList<Room> knownRooms = new ArrayList<Room>();
	
	@Override
	/**
	 * Drives the robot towards the exit given it exists and 
	 * given the robot's energy supply lasts long enough. 
	 * @return true if driver successfully reaches the exit, false otherwise
	 * @throws exception if robot stopped due to some problem, e.g. lack of energy
	 */
	public boolean drive2Exit() throws Exception {
		if(DEV_MODE) {
			robot.setBatteryLevel(Float.MAX_VALUE);
		}
		cellVisits = new int[this.w][this.h]; // visit count. counts the times the robot has entered this cell 
		// TODO: handle starting-in-room case
		while (true) {
			// if we just entered a room, fast-forward through it to its least traveled exit,
			Room r = null;
			if(robot.hasRoomSensor() && robot.isInsideRoom()) {
				r = findMappedRoom(getCurPos());
				if (r == null) {
					// map room
					r = new RoomMapper().mapRoom();
					assert r != null;
					dbg("Just mapped room: ");
					dbg(r.toString());
					knownRooms.add(r);
				} else {
					dbg("In known room.");
				}
				assert r != null;
				int[] exit = r.leastUsedExit();
				moveToDoor(exit);
			} else {
				assert r == null : "Room must be null when not in room.";
			}
			// check all directions, go for least visited cell in possible cells.
			// if we just fast-forwarded to the exit of a room, only consider
			// cells outside the room.
			ArrayList<Direction> possibilities = new ArrayList<Direction>();
			// check possibilities
			for(Direction d: Direction.values()) {
				// check distance to obstacle in every direction
				int dist = robot.distanceToObstacle(d); 

				// check for exit
				// if the robot can see the exit, go straight to exit
				if(dist == Integer.MAX_VALUE) {
					turn(d);
					while(!robot.isAtExit()) {
						robot.move(1, false);
					}
					return true;
				}
				// skip this direction as the robot is blocked
				if(dist == 0) {
					dbg("Blocked direction: " + d);
					continue;
				}
				if(r != null) {
					int[] pos = getCurPos();
					CardinalDirection cd = d.addTo(robot.getCurrentDirection());
					int[] offset = cd.getDirection();
					int[] newPos = new int[] {pos[0] + offset[0], pos[1] + offset[1]};
					if(r.contains(newPos)) {
						dbg("Skipping in-room direction.");
						continue;
					}
					// eliminate possibilities in the room we just exited
				}
				possibilities.add(d);
			}
			
			// sanity check
			if(possibilities.size() == 0) {
				throw new RuntimeException("Explorer trapped on all sides.");
			}
			
			// check all possibilities and find the least traveled cell, breaking ties with randomization
			int ties = 1; // count ties so we could break ties evenly using Reservoir Sampling
			Direction toGo = possibilities.get(0);
			int max = Integer.MAX_VALUE;
			for(Direction d : possibilities) {
				int cur = countVisits(d);
				if (cur == max) {
					ties ++;
					int randomNum = ThreadLocalRandom.current().nextInt(0, ties);
					if(randomNum == 0) {
						toGo = d;
					}
				}
				else if (cur < max) {
					ties = 1;
					toGo = d;
					max = cur;
				}
			}
			
			// turn appropriately
			turn(toGo);
			
			// double mark dead-ends to prevent circling and improve performance
			if(possibilities.size() == 1) {
				cellVisits[getCurPos()[0]][getCurPos()[1]] += 1;
			}
			// increment current counter and move
			move();
		}
	}
	
	/**
	 * Check if the robot is in an already-mapped room, and return it if so.
	 * 
	 * @param x x-coordinate of the robot
	 * @param y y-coordinate of the robot
	 * @return Room object describing the room the robot is in.
	 */
	Room findMappedRoom(int x, int y) {
		for(Room r : knownRooms) {
			if (r.contains(x, y)){
				return r;
			}
		}
		return null;
	}
	
	/**
	 * Check if the robot is in an already-mapped room, and return it if so.
	 * 
	 * @param pos x and y coordinates of the robot
	 * @return Room object describing the room the robot is in.
	 */
	Room findMappedRoom(int[] pos) {
		return findMappedRoom(pos[0], pos[1]);
	}
		
	
	/**
	 * 
	 * @param dn direction the robot is turning to
	 * @return the updated visit count of the cell 
	 */
	int countVisits(Direction dn) {
		CardinalDirection face = robot.getCurrentDirection();
		CardinalDirection cd = dn.addTo(face);
		int[] offset = cd.getDirection();
		int[] pos = null;
		try {
			pos = robot.getCurrentPosition();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(String.format("Counting visits for (%d, %d)", pos[0] + offset[0], pos[1] + offset[1]));
		return cellVisits[pos[0] + offset[0]][pos[1] + offset[1]];
	}
	
	/**
	 * helper method
	 * 
	 * increments the current cell's visit counter, and move forward
	 */
	private void move() {
		int[] pos;
		try {
			pos = robot.getCurrentPosition();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		int x = pos[0];
		int y = pos[1];
		
		cellVisits[x][y] += 1;
		robot.move(1, false);
	}
	private void move(int count) {
		for(int i = 0; i < count; i++) {
			move();
		}
	}
	
	/* room-related stuff */

	private void moveToDoor(int[] door) {
		assert robot.isInsideRoom() : "moveToDoor only works inside rooms";
		
		CardinalDirection directionToGo = robot.getCurrentDirection();
		
		for(int i = 0; i < 2; i ++) { // move toward the door in a straight line, twice
			if(Arrays.equals(door, getCurPos())) {
				robot.rotate(Turn.AROUND);
				return;
			}
			int[] pos = getCurPos();
			int[] diff = new int[] {door[0] - pos[0], door[1] - pos[1]};
			int dp = dot(diff, directionToGo.getDirection());
			assert dp >= 0 : "dp less than zero on trial " + i + "dp: "+ dp; // we should be at room's edge, so all doors are foward
			if(dp > 0) { // if moving forward gets us closer, do so
				move(dp);
			} else {
				// turn, and then move
				directionToGo = directionToGo.rotateClockwise();
				dp = dot(diff, directionToGo.getDirection());
				if (dp > 0) {
					robot.rotate(Turn.LEFT); // stupid mothefuggin direction inversion
					move(dp);
				} else if (dp < 0) {
					robot.rotate(Turn.RIGHT);
					move(-dp);
				} else {
					int[] face = robot.getCurrentDirection().getDirection();
					throw new RuntimeException(
							String.format("Error trying to move to door at (%d, %d); current position: (%d, %d), face: (%d, %d)",
									door[0], door[1], pos[0], pos[1], face[0], face[1])
							);
				}
			}
		}
		assert Arrays.equals(getCurPos(), door);
	}

	private class Range
	{
	    public int low;
	    public int high;

	    public Range(int low, int high){
	        this.low = low;
	        this.high = high;
	    }

	    public boolean contains(int number){
	        return (number >= low && number <= high);
	    }
	}
	
	private class Room {
		final Range xrange;
		final Range yrange;
		final ArrayList<int[]> doors;
		int[] doorUseCount;
		/**
		 * Constructor for room; this is 
		 */
		public Room(int xmin, int ymin, int xmax, int ymax, ArrayList<int[]> doors) {
			xrange = new Range(xmin, xmax);
			yrange = new Range(ymin, ymax);
			this.doors = doors;
			doorUseCount = new int[doors.size()];
		}
		
		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append(String.format("Room: (%d, %d) to (%d, %d):\n",
					xrange.low, yrange.low, xrange.high, yrange.high));
			for(int[] door : doors) {
				sb.append(String.format("\tDoor: (%d, %d)\n", door[0], door[1]));
			}
			return sb.toString();
			
		}
		
		/**
		 * Check if room contains given position.
		 *
		 * @param x x-coordinate of the given position
		 * @param y y-coordinate of the given position
		 * @return true if the cell (x, y) is strictly within the room.
		 */
		public boolean contains(int x, int y) {
			return xrange.contains(x) && yrange.contains(y);
		}
		public boolean contains(int[] pos) {
			return contains(pos[0], pos[1]);
		}
		
		public int[] leastUsedExit() {
			int valMin = Integer.MAX_VALUE;
			int idxMin = 0;
			for(int i = 0; i < doorUseCount.length; i++) {
				if(doorUseCount[i] < valMin) {
					idxMin = i;
					valMin = doorUseCount[i];
				}
			}
			doorUseCount[idxMin] += 1;
			return doors.get(idxMin);
		}
	}
	
	/**
	 * This factory class handles the creation of rooms.
	 * Assumes that you are at the entrance of the room, still facing the direction
	 * you came in.
	 * 
	 * @author xren
	 *
	 */
	private class RoomMapper{
		int xmin = Integer.MAX_VALUE;
		int ymin = Integer.MAX_VALUE;
		
		int xmax = Integer.MIN_VALUE;
		int ymax = Integer.MIN_VALUE;
		
		int cornerCount; // count the number of right turns we made at corners; helps with pathological cases
		
		/**
		 * Map the room.
		 * 
		 * @return gui.Explorer.Room object describing the room 
		 */
		public Room mapRoom() {
			dbg("Mapping room.");
			assert robot.isInsideRoom();
			// follow wall on the left
			cornerCount = 0;
			robot.rotate(Turn.LEFT);
			int [] entrance = getCurPos(); // this is where the robot came in from
			ArrayList<int[]> doorsFound = new ArrayList<int[]>(); // store a list of doors
			int[] door; // current door
			while(true) {
				// find a door, and save it
				door = findNextDoor();
				doorsFound.add(door);
				dbg(String.format("Entrance: (%d, %d); Current door: (%d, %d)", entrance[0], entrance[1], door[0], door[1]));
				if(Arrays.equals(door, entrance)) {
					if(cornerCount < 4) {
						dbg("We are discovering the same door twice. Continue circling until we discover all the other doors and get back to the entrance.");
						// we have struck a pathological case where we found another door on the cell we just entered fro
						continue; // if we break return the room here, mapRoom will be called again on the same room in an endless cycle  
					}
					break;
				}
				if(cornerCount >= 5) {
					// we have done at least a full circle without finding our original entrance
					// this means that we started in the middle of the room, and that we should start with
					// the entrance we're on and relaunch the door search from there
					cornerCount = 0;
					entrance = getCurPos();
					doorsFound = new ArrayList<int[]>();
				}
			}
			robot.rotate(Turn.RIGHT); // after mapping a room, turn the robot back to face into the room
			return new Room(xmin, ymin, xmax, ymax, doorsFound);
		}
		
		/**
		 * Take robot forward until room boundary or door found.
		 * Assumes that the robot is on the inside of a door's 
		 * threshold, and is facing left compared to the direction
		 * it entered.
		 * 
		 * The robot turns right automatically on encountering a wall.
		 * 
		 * @return the doors found
		 */
		private int[] findNextDoor() { 
			while(true) {  // repeat until we find a door
				// go forward until we hit a wall or find a room
				int wallDistance = robot.distanceToObstacle(Direction.FORWARD);
				for(int i = 0; i < wallDistance; i ++) {
					// move a step without incrementing the cell's counter,
					// since we're not actually going to commit to exploring the area:
					// we're just circling the room
					robot.move(1, false);
					// if we got outside a room, get back, and register the place
					// as a door; turn right and increment cornerCount since we
					// are along a wall and hence must have just hit a corner
					if(!robot.isInsideRoom()) {
						robot.rotate(Turn.AROUND);
						robot.move(1, false); 
						robot.rotate(Turn.LEFT); // around + left = right
						cornerCount += 1;
						int[] pos = getCurPos();
						// charging out of a room means that we have hit a corner,
						// so we check-update the room boundaries
						xmin = min(xmin, pos[0]);
						ymin = min(ymin, pos[1]);
						xmax = max(xmax, pos[0]);
						ymax = max(ymax, pos[1]);
						return getCurPos(); // return if found a door at the end of the wall
					}
					if(robot.distanceToObstacle(Direction.LEFT) > 0) {
						return getCurPos(); // return if found a door when walking along the wall
					}
				}
				// end of road; at corner; update room boundaries, turn right, increment corner count.
				int[] pos = getCurPos();
				xmin = min(xmin, pos[0]);
				ymin = min(ymin, pos[1]);
				xmax = max(xmax, pos[0]);
				ymax = max(ymax, pos[1]);
				robot.rotate(Turn.RIGHT);
				cornerCount += 1;
			}
			
		}
	}
	


	/**
	 * computes a dot product of two vectors, expressed as arrays.
	 * 
	 * @param a one vector to multiply
	 * @param b the other vector to multiply
	 * @return int, their dot product
	 */
	private int dot(int[] a, int[] b) {
		assert a.length == b.length : "the two arrays to be multiplied should have the same length";
		int dp = 0;
		// multiply each a_i, b_i pair, and add it to the dot product, the result
		for(int i = 0; i < a.length; i ++) {
			dp += a[i] * b[i];
		}
		return dp;
	}
	
	/**
	 * helper method to get current position and handle possible errors
	 * @return current positions
	 */
	private int[] getCurPos() {
		try {
			return robot.getCurrentPosition().clone();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
	}
	
	/**
	 * print debug message
	 */
	private void dbg(Object msg) {
		if(DriverExplorer.DEV_MODE) {
			System.out.println("[Explorer]" + msg.toString());
		}
	}
}
