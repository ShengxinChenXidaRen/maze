package gui;

import static java.lang.Math.abs;

import generation.CardinalDirection;
import generation.Direction;
import gui.Robot.Turn;
/**
 * Class: 
 * Wizard
 * 
 * Responsibility:
 * Solve the maze based on information on distance
 * 
 * Collaborator:
 * BasicRobot, Distance
 *
 * @author chenshengxin
 *
 */
public class Wizard extends Driver{
	public final DriverType driverType = DriverType.WIZARD;
	
	@Override
	public boolean drive2Exit() throws Exception {
		if(distance == null) {
			throw new Exception("Distance not provided to wizard.");
		}
		// while not at the exit
		while (!robot.isAtExit()) {
			// handle exception
			step();
		}
		return true;
	}
	
	
	void step() throws Exception{
		int [] a = robot.getCurrentPosition();
		// find the adjacent cell with a shorter distance to exit
		int [] b = getNeighborCloserToExit(a[0], a[1]);
		log(String.format("Stepping from (%d,%d) to (%d, %d)", a[0], a[1], b[0], b[1]));
		
		CardinalDirection current = robot.getCurrentDirection();
		CardinalDirection  target = getDiff(a, b);
		log("Facing " + current);
		log("Need to go " + target);
		Direction relativeDirection = current.difference(target);
		log("Neet to turn in direction " + relativeDirection);
		
		switch(relativeDirection) {
		case BACKWARD:
			robot.rotate(Turn.AROUND);
			break;
		case FORWARD:
			break;
		case LEFT:
			robot.rotate(Turn.LEFT);
			break;
		case RIGHT:
			robot.rotate(Turn.RIGHT);
			break;
		default:
			break;
		}
		log("Going in CardinalDirection " + robot.getCurrentDirection());
		robot.move(1, false);
	}
	
	private int[] getNeighborCloserToExit(int x, int y) {
		// corner case, (x,y) is exit position
		if (distance.isExitPosition(x, y))
			return null;
		// find best candidate
		int dnext = distance.getDistanceValue(x, y);
		int[] result = new int[2] ;
		int[] dir;
		for (CardinalDirection cd: CardinalDirection.values()) {
			if (robot.distanceToObstacle(robot.getCurrentDirection().difference(cd)) == 0) 
				continue; // there is a wall
			// no wall, let's check the distance
			dir = cd.getDirection();
			int dn = distance.getDistanceValue(x+dir[0], y+dir[1]);
			if (dn < dnext) {
				// update neighbor position with min distance
				result[0] = x+dir[0] ;
				result[1] = y+dir[1] ;
				dnext = dn ;
			}	
		}
		// expectation: we found a neighbor that is closer
		assert(distance.getDistanceValue(x, y) > dnext) : 
			"cannot identify direction towards solution: stuck at: " + x + ", "+ y ;
		// since assert statements need not be executed, check it 
		// to avoid giving back wrong result
		return (distance.getDistanceValue(x, y) > dnext) ? result : null;
	}
	
	
	/**
	 * Get direction needed to turn to go from a = {x,y} to b = {x,y}
	 * @returns CardinalDirection needed to get from a to b, null if no turn needed
	 */
	private CardinalDirection getDiff(int[] a, int[] b) {
		final int dx = b[0] - a[0];
		final int dy = b[1] - a[1];
		assert abs(dx) + abs(dy) == 1 : "The cells should differ by exactly one taxi-cab distance unit";
		return CardinalDirection.getDirection(dx, dy);
	}

	private void log(Object o) {
		System.out.println("[Wizard]: " + o.toString());

	}
}
