package gui;

import generation.Direction;
import generation.Distance;
import gui.Robot.Turn;

public abstract class Driver implements RobotDriver{
	protected Robot robot;
	private float initialEnergy;
	public final DriverType driverType = null;
	
	protected int h;
	protected int w;
	enum DriverType {WIZARD, WALLFOLLOWER, EXPLORER, PLEDGE, MANUAL};    
	protected Distance distance;
	/**
	 * Assigns a robot platform to the driver. 
	 * The driver uses a robot to perform, this method provides it with this necessary information.
	 * @param r robot to operate
	 */
	public void setRobot(Robot r) {
		this.robot = r;
		this.initialEnergy = robot.getBatteryLevel();
	}
	
	/**
	 * Provides the robot driver with information on the dimensions of the 2D maze
	 * measured in the number of cells in each direction.
	 * @param width of the maze
	 * @param height of the maze
	 * @precondition 0 <= width, 0 <= height of the maze.
	 */
	public void setDimensions(int width, int height) {
		assert width >= 0;
		assert height >= 0;
		this.w = width;
		this.h = height;
	}
	/**
	 * Provides the robot driver with information on the distance to the exit.
	 * Only some drivers such as the wizard rely on this information to find the exit.
	 * @param distance gives the length of path from current position to the exit.
	 * @precondition null != distance, a full functional distance object for the current maze.
	 */
	public void setDistance(Distance distance) {
		assert distance != null;
		this.distance = distance;
	}
	/**
	 * Drives the robot towards the exit given it exists and 
	 * given the robot's energy supply lasts long enough. 
	 * @return true if driver successfully reaches the exit, false otherwise
	 * @throws exception if robot stopped due to some problem, e.g. lack of energy
	 */
	public boolean drive2Exit() throws Exception {
		return true;
	}
	
	/**
	 * Returns the total energy consumption of the journey, i.e.,
	 * the difference between the robot's initial energy level at
	 * the starting position and its energy level at the exit position. 
	 * This is used as a measure of efficiency for a robot driver.
	 */
	public float getEnergyConsumption() {
		return initialEnergy - robot.getBatteryLevel();
	}
	
	/**
	 * Returns the total length of the journey in number of cells traversed. 
	 * Being at the initial position counts as 0. 
	 * This is used as a measure of efficiency for a robot driver.
	 */
	public int getPathLength() {
		return robot.getOdometerReading();
	}
	
	protected void turn(Direction d) {
		Turn turn;
		switch(d) {
		case BACKWARD:
			turn = Turn.AROUND;
			break;
		case LEFT:
			turn = Turn.LEFT;
			break;
		case RIGHT:
			turn = Turn.RIGHT;
			break;
		case FORWARD:
			turn = null;
			return;
		default:
			throw new RuntimeException("Enum value invalid: direction to turn in");
		}
		robot.rotate(turn);
	}

	public static Driver getDriver(DriverType dt) {
		switch(dt) {
		case WIZARD:
			return new Wizard();
		case EXPLORER:
			return new DriverExplorer();
		case WALLFOLLOWER:
			return new WallFollower();
		case PLEDGE:
			return new Pledge();
		case MANUAL:
			return new ManualDriver();
		default:
			System.out.println("No driver is selected, default to manual driver.");
			return new ManualDriver();

		}
	}
}
