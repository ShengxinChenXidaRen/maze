package gui;

import java.awt.GridLayout;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JTextField;

import generation.Order.Builder;
import gui.Driver.DriverType;



public class MenuPanel extends Panel{
	
	public Builder builder = Builder.DFS;
	public Driver.DriverType driverType = DriverType.WIZARD;
	public int level = 0;
	public Integer seed = null;
	
	JComboBox<Builder> generationList;
	JComboBox<DriverType> driverList;
	JComboBox<Integer> levelList;
	JTextField seedBox;
	JButton startButton;
	
	
	public MenuPanel(Controller controller) {
	    Integer[] levelOptions = new Integer[15]; 
	    int inc=1;
	    for(int i=0;i<15;i++){
	        levelOptions[i]= inc;
	        inc++;
	    }
	    
	    generationList = new JComboBox<Builder>(Builder.values());
	    driverList = new JComboBox<DriverType>(DriverType.values());
	    levelList = new JComboBox<Integer> (levelOptions);
	    seedBox = new JTextField("seed", 2);
		startButton = new JButton("Start");
	    
		GridLayout menuLayout = new GridLayout(0,1+1+1+1+1);
		
		setLayout(menuLayout);
		
		add(generationList);
		generationList.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
	        JComboBox<Builder> cb = (JComboBox<Builder>)e.getSource();
	        builder = (Builder)cb.getSelectedItem();
			}});
		
		add(driverList);
		driverList.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
		        JComboBox<DriverType> cb = (JComboBox<DriverType>)e.getSource();
		        driverType = (DriverType)cb.getSelectedItem();
		        }
			});
		
		add(levelList);
		levelList.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JComboBox cb = (JComboBox)e.getSource();
		        int selected = (int)cb.getSelectedItem();
		        level = selected;
			}
		});
		
		add(seedBox);
		
		add(startButton);
		startButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.setBuilder(builder);
				controller.setDriverType(driverType);
				controller.setSeed(computeSeed(seedBox.getText()));
				controller.switchFromTitleToGenerating(level);
			};
			});
	}
	
	Integer computeSeed(String seedString) {
		try {
			// if seed is an integer
			seed = Integer.valueOf(seedString);
		} catch(NumberFormatException nfe){
			// use hashcode if seed is not an integer
			seed = seedString.hashCode();
		}
		return seed;
	}
	

}
