package gui;

import java.awt.Container;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import gui.Constants.UserInput;


/**
 * Class implements a translation for the user input handled by the MazeController class. 
 * The MazeApplication attaches the listener to the GUI, such that user keyboard input
 * flows from GUI to the listener.keyPressed to the MazeController.keyDown method.
 *
 * This code is refactored code from Maze.java by Paul Falstad, 
 * www.falstad.com, Copyright (C) 1998, all rights reserved
 * Paul Falstad granted permission to modify and use code for teaching purposes.
 * Refactored by Peter Kemper
 */
public class SimpleKeyListener implements KeyListener {

	private Container parent ;
	private Controller controller ;
	
	SimpleKeyListener(Container parent, Controller controller){
		this.parent = parent;
		this.controller = controller;
	}
	/**
	 * Translate keyboard input to the corresponding operation for 
	 * the Controller.keyDown method.
	 */
	@Override
	public void keyPressed(KeyEvent arg0) {
		int key = arg0.getKeyChar();
		int code = arg0.getKeyCode();
		
		//Possible operations for UserInput based on enum
		// {ReturnToTitle, Start, 
		// Up, Down, Left, Right, Jump, 
		// ToggleLocalMap, ToggleFullMap, ToggleSolution, 
		// ZoomIn, ZoomOut };
		UserInput uikey = UserInput.parseKeyEvent(arg0);
		
		if(uikey == null) {
			return;
		}
		
		
		int difficultyLevel = 0;
		if(uikey == UserInput.Start) {
			int difficultyKeyCode = arg0.getKeyChar();
			if(difficultyKeyCode == KeyEvent.CHAR_UNDEFINED) {
				difficultyKeyCode = arg0.getKeyCode();
			}
			
			// check ranges of values as possible selections for skill level
			if (difficultyKeyCode >= '0' && difficultyKeyCode <= '9') {
				difficultyLevel = key - '0';
			} else
			if (difficultyKeyCode >= 'a' && difficultyKeyCode <= 'f') {
				difficultyLevel = key - 'a' + 10;
			}
		}			
		
		assert (0 <= difficultyLevel && difficultyLevel <= 15);		
		// feed user input into controller
		// value is only used in combination with uikey == Start
		controller.keyDown(uikey, difficultyLevel);
		parent.repaint() ;
	}
	@Override
	public void keyReleased(KeyEvent arg0) {
		// nothing to do
		
	}
	@Override
	public void keyTyped(KeyEvent arg0) {
		// NOTE FOR THIS TYPE OF EVENT IS getKeyCode always 0, so Escape etc is not recognized	
		// this is why we work with keyPressed
	}

}

