package gui;

import generation.Direction;
import gui.Robot.Turn;

/**
 * 
 * @author chenshengxin
 *
 * Class:
 * WallFollower
 * 
 * Responsibility:
 * Solve the maze by following the left wall
 * 
 * Collaborator: 
 * BasicRobot
 * 
 */

public class WallFollower extends Driver {
	
	int countturn = 0;
	
	void followWall() throws Exception {
		if (robot.hasStopped()) throw new Exception("The robot has stopped.");
		// if there's no wall on the left
		// the robot should go left
		
		
		if(robot.distanceToObstacle(Direction.LEFT) != 0) {
			if (countturn != 4) {
				robot.rotate(Turn.LEFT);
				countturn ++;
				robot.move(1, false);
				return;
			}
			else {
				robot.rotate(Turn.AROUND);
				int distToWall = robot.distanceToObstacle(Direction.LEFT);
				robot.rotate(Turn.LEFT);
				countturn = 0;
				robot.move(distToWall, false);
				return;
			}
		} 
		// if both the left and front are blocked
		// adjust the direction by turning right
		else while (robot.distanceToObstacle(Direction.FORWARD)==0) {
			robot.rotate(Turn.RIGHT);
		}
		// move forward in current direction
		countturn = 0;
		robot.move(1, false);
		return;
	}
	
	
	@Override
	/**
	 * Drives the robot towards the exit given it exists and 
	 * given the robot's energy supply lasts long enough. 
	 * @return true if driver successfully reaches the exit, false otherwise
	 * @throws exception if robot stopped due to some problem, e.g. lack of energy
	 */
	public boolean drive2Exit() throws Exception {
		// while not at the exit
		while (!robot.isAtExit()) {
			// followWall handles the exception
			followWall();
		}
		return true;
	}

}
