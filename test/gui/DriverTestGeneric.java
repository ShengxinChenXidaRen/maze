package gui;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import generation.Direction;
import gui.Driver.DriverType;
import gui.Robot.Turn;

/**
 * This class contains generic tests that should be run for each diver type.
 * 
 * @author xren
 *
 */
public class DriverTestGeneric{	
	
	/**
	 * Helper function used to run the driver through a maze
	 * generated with a set of pre-configured test conditions.
	 * 
	 * @param t a container object of type TestConfig that prescribes
	 * the maze and how the driver should be placed in it
	 */
	void tryNavigatingMaze(TestConfig t, Driver d) {
		if(d.driverType == DriverType.WALLFOLLOWER) {
			assumeTrue(t.isPerfect()); // skip test case if attempting wall follower on imperfect maze
		}
		FakeMazeApplication fma = new FakeMazeApplication(t);
		fma.start(null);
		Direction diff = d.robot.getCurrentDirection().difference(t.initialFace);
		switch(diff) {
		case BACKWARD:
			d.robot.rotate(Turn.AROUND);
			break;
		case FORWARD:
			// do nothing
			break;
		case LEFT:
			d.robot.rotate(Turn.LEFT);
			break;
		case RIGHT:
			d.robot.rotate(Turn.RIGHT);
			break;
		default:
			throw new RuntimeException("Invalid enum case");		
		}
		d.robot.setBatteryLevel(t.allottedEnergy);
		try {
			assertTrue(d.drive2Exit(), "The driver should report that it was able to drive to exit.");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertTrue(d.robot.isAtExit(), "The robot should be at the exit.");
		assertFalse(d.robot.hasStopped(), "10 thousand energy should be enough to bring the robot through a maze of difficulty 5.");
		fma.dispose();
	}
		

	/**
	 * Test if the driver can drive to exit on a pathological case:
	 * te robot is standing on a cell with two doors,
	 * one behind it, from which it entered,
	 * the other to the left of it.
	 * 
	 * This case is catered towards the room-skipping algorithm of Explorer,
	 * but I run it for the others anyway. Wizard, Pledge, and Explorer should
	 * all be able to pass this case.
	 * 
	 * This case may cause an infinite loop with an unpatched room-skipping explorer.
	 */
	@ParameterizedTest
	@EnumSource(value=DriverType.class, names={"WIZARD", "EXPLORER", "WALLFOLLOWER", "PLEDGE"})
	void testTwoDoorsSameCell(DriverType dt) {
		TestConfig t = TestConfig.getTestConfig("TwoDoorsSameCell");
		Driver d = Driver.getDriver(dt);
		t.setDriver(d);
		tryNavigatingMaze(t, d);
	}
	
	/**
	 * Make sure the robot doesn't get confused if it is placed in the center of a room
	 */
	@ParameterizedTest
	@EnumSource(value=DriverType.class, names={"WIZARD", "EXPLORER", "WALLFOLLOWER", "PLEDGE"})
	void testPlacedInRoom(DriverType dt) {
		TestConfig t = TestConfig.getTestConfig("PlacedInRoom");
		Driver d = Driver.getDriver(dt);
		t.setDriver(d);
		tryNavigatingMaze(t, d);
	}
}
