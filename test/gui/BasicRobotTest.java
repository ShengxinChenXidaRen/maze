package gui;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;
import java.util.stream.Stream;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.MethodSource;

import generation.CardinalDirection;
import generation.Direction;
import gui.Robot.Turn;
/**
 * Junit test cases for BasicRobot implementation of Robot interface.
 * Class contains a number of black box tests.
 * Test scenario: uses DummyController which generates a predetermined map
 * 
 * @author Shengxin Chen, Xida Ren
 *
 */

class BasicRobotTest {
	FakeMazeApplication fma;
	Robot r;
	
	/**
	 * Initialize a maze with map looking like this:
	 * 
	 * +---+   +---+---+
	 * |0,3|1,3 2,3    |
	 * +   +---+   +   +
	 * |       |       |
	 * +   +---+   +   +
	 * |0,1 1,1 2,1    |
	 * +   +   +   + 
	 * |0,0 1,0 2,0    |
	 * +---+---+---+---+
	 * 
	 * The initial position is (1, 2), facing east
	 * (0, 3) is marked as inside room
	 */
	@BeforeEach
	public void setUp() {
		TestConfig t = TestConfig.getTestConfig("TestMaze1");
	    r = new BasicRobot();
	    t.setRobot(r);
		fma = new FakeMazeApplication(t);
	    fma.start(null);
	}
	
	@AfterEach
	public void tearDown() {
		fma.dispose();
	}
	
	/**
	 * Test method to check energy consumption of rotating left
	 */
	@Test
	void testEnergyLossTurnLeft() {
		final float energy = r.getEnergyForFullRotation();
		r.setBatteryLevel(energy);
		for (int i=0; i < 4; i++) {
			assertFalse(r.hasStopped());
			r.rotate(Turn.LEFT);
		}
		r.move(1, true);
		assertTrue(r.hasStopped());
		assertTrue(r.getBatteryLevel() == 0);
	}
	
	/**
	 * Test method to check energy consumption of rotating right
	 */
	@Test
	void testEnergyLossTurnRight() {
		final float energy = r.getEnergyForFullRotation();
		r.setBatteryLevel(energy);
		for (int i=0; i < 4; i++) {
			assertFalse(r.hasStopped());
			r.rotate(Turn.RIGHT);
		}
		r.move(1, true);
		assertTrue(r.hasStopped());
		assertTrue(r.getBatteryLevel() == 0);
	}
	
	/**
	 * Test method to check energy consumption of turning around
	 */
	@Test
	void testEnergyLossTurnAround() {
		final float energy = r.getEnergyForFullRotation();
		r.setBatteryLevel(energy);
		for (int i=0; i < 2; i++) {
			assertFalse(r.hasStopped());
			r.rotate(Turn.AROUND);
		}
		r.move(1, true);
		assertTrue(r.hasStopped());
		assertTrue(r.getBatteryLevel() == 0);
	}
	

	/**
	 * Test method to check energy consumption of moving forward
	 * Energy level is set to be depleted after one step forward
	 */
	@Test
	void testEnergyLossMove() {
		final float energy = r.getEnergyForFullRotation() / 2 + r.getEnergyForStepForward();
		r.setBatteryLevel(energy);
		r.rotate(Turn.AROUND);
		r.move(1, true);
		r.move(1, true);
		assertTrue(r.hasStopped());
		assertTrue(r.getBatteryLevel() == 0);
		assertTrue(r.getOdometerReading() == 1);
	}
	
    @ParameterizedTest
    @EnumSource(value = Turn.class)
	void testNoEnergyRotateAttempt(Turn turn) {
		CardinalDirection initialDirection = r.getCurrentDirection();
		r.setBatteryLevel(0);
		r.rotate(turn);
		assert(r.getCurrentDirection() == initialDirection);
	}
	
	/**
	 * Test method to check distanceToObstacle(Direction direction)
	 * The initial position is surrounded by 3 walls (east, north and south)
	 * It is 1 step away from the west wall
	 */
	@Test
	void testSenseObstacle() {
		assertTrue(r.distanceToObstacle(Direction.FORWARD) == 0);
		assertTrue(r.distanceToObstacle(Direction.LEFT) == 0);
		assertTrue(r.distanceToObstacle(Direction.RIGHT) == 0);
		assertTrue(r.distanceToObstacle(Direction.BACKWARD) == 1);
	}

	/**
	 * Test method to check energy consumption of robot sensor
	 */
	@Test
	void testEnergyLossSense() {
		final float energy = 1;
		r.setBatteryLevel(energy);
		r.distanceToObstacle(Direction.FORWARD);
		assertTrue(r.getBatteryLevel() == 0);
	}
	
	/**
	 * Test method to check the room sensor
	 * The robot is moved to (0, 3), which is marked as inside room in DummyController
	 */
	@Test
	void testRoomSensor() {
		assertFalse(r.isInsideRoom());
		r.rotate(Turn.AROUND);
		r.move(1, false);
		assertFalse(r.isInsideRoom());
		r.rotate(Turn.RIGHT);
		r.move(1, false);
		assertTrue(r.isInsideRoom());
		r.rotate(Turn.AROUND);
		r.move(1, false);
		assertFalse(r.isInsideRoom());
		r.move(1, false);
		assertFalse(r.isInsideRoom());
	}
	
	/**
	 * Test method for CanSeeExit()
	 * @throws Exception 
	 * 
	 */
	@Test
	void testCanSeeExit() throws Exception {
		r.rotate(Turn.AROUND);
		r.move(1, true);
		r.rotate(Turn.LEFT);
		r.move(2, true);
		r.rotate(Turn.LEFT);
		r.move(3, true);
		r.rotate(Turn.LEFT);
		r.move(2, true);
		// at (2, 3), cannot see exit yet
		assertArrayEquals(new int[] {3,2}, r.getCurrentPosition());
		assertFalse(r.canSeeExit(Direction.LEFT));;
		r.move(1, true);
		// at (1, 3), exit is on the right
		assertArrayEquals(new int[] {3,3}, r.getCurrentPosition());
		assertTrue(r.canSeeExit(Direction.RIGHT));
	}
	
	/**
	 * Test method for AtExit()
	 */
	@Test
	void testAtExit() {
		r.rotate(Turn.AROUND);
		r.move(1, true);
		r.rotate(Turn.LEFT);
		r.move(2, true);
		r.rotate(Turn.LEFT);
		r.move(3, true);
		r.rotate(Turn.LEFT);
		r.move(2, true);
		// at (3, 2)
		assertFalse(r.isAtExit());
		r.move(1, true);
		// at (3, 3)
		assertTrue(r.isAtExit());
	}
	
	/**
	 * Test method for distanceToObstacle()
	 * The robot is moved to (1, 3), which is 1 step away from the exit
	 * The method should return MAX_VALUE as specified
	 * @throws Exception 
	 */
	@Test
	void testSenseObstacleAtExit() throws Exception {
		r.rotate(Turn.AROUND);
		r.move(1, true);
		r.rotate(Turn.LEFT);
		r.move(2, true);
		r.rotate(Turn.LEFT);
		r.move(2, true);
		r.rotate(Turn.LEFT);
		r.move(3, true);
		
		assertArrayEquals(new int[] {2,3}, r.getCurrentPosition());
		assertEquals(0, r.distanceToObstacle(Direction.FORWARD));
		assertEquals(Integer.MAX_VALUE, r.distanceToObstacle(Direction.RIGHT));
		
		r.rotate(Turn.RIGHT);
		r.move(1, true);
		
		assertArrayEquals(new int[] {3,3}, r.getCurrentPosition());
		assertEquals(Integer.MAX_VALUE, 
				r.distanceToObstacle(Direction.FORWARD));
		assertEquals(3, 
				r.distanceToObstacle(Direction.RIGHT));

	}
	
	/**
	 * Test method for resetOdometer()
	 * After resetting, the odometer reading should be zero
	 */
	@Test
	void testResetOdometer() {
		r.rotate(Turn.AROUND);
		r.move(1, true);
		r.rotate(Turn.LEFT);
		r.move(2, true);
		r.rotate(Turn.LEFT);
		r.move(2, true);
		assertFalse(r.getOdometerReading() == 0);
		r.resetOdometer();
		assertTrue(r.getOdometerReading() == 0);
		}
	
	/**
	 * Test if getCurrentDirection() considers both the first person perspective and robot direction
	 */
	@Test
	void testDirection() {
		assertTrue(r.getCurrentDirection() == CardinalDirection.East);
		r.rotate(Turn.AROUND);
		assertTrue(r.getCurrentDirection() == CardinalDirection.West);
		r.move(1, true);
		r.rotate(Turn.LEFT);
		assertTrue(r.getCurrentDirection() == CardinalDirection.North);
		r.move(2, true);
		r.rotate(Turn.LEFT);
		assertTrue(r.getCurrentDirection() == CardinalDirection.East);
		r.move(2, true);
		r.rotate(Turn.LEFT);
		assertTrue(r.getCurrentDirection() == CardinalDirection.South);
		r.move(3, true);
		r.rotate(Turn.LEFT);
		assertTrue(r.getCurrentDirection() == CardinalDirection.West);
		r.move(1, true);
	}
	
	/**
	 * Test if getCurrentPosition() tracks the robot position correctly
	 */
	@Test
	void testPosition() {
		r.rotate(Turn.AROUND);
		r.move(1, true);
		try {
			assertTrue(r.getCurrentPosition()[0] == 0);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			assertTrue(r.getCurrentPosition()[1] == 2);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		r.rotate(Turn.LEFT);
		r.move(2, true);
		r.rotate(Turn.LEFT);
		r.move(2, true);
		r.rotate(Turn.LEFT);
		r.move(3, true);
		r.rotate(Turn.LEFT);
		r.move(1, true);
		try {
			assertTrue(r.getCurrentPosition()[0] == 1);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			assertTrue(r.getCurrentPosition()[1] == 3);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@ParameterizedTest
	@MethodSource("allCombinationsManualAndDirections")
	void testRunningIntoWall(boolean manual, Turn turn) {
		r.setBatteryLevel(3000);
		r.rotate(turn);
		final int[] oldPos;
		try {
			oldPos = r.getCurrentPosition();
		} catch (Exception e) {fail("Get old position failed.");return;}
		final float oldEnergy = r.getBatteryLevel();
		final CardinalDirection oldFace = r.getCurrentDirection();

		boolean facingWall = fma.controller.getMazeConfiguration().hasWall(
				oldPos[0], oldPos[1], r.getCurrentDirection());
		
		r.move(1, manual);
		
		
		if(facingWall) {
			// position shouldn't change
			try {
				assertArrayEquals(oldPos, r.getCurrentPosition());
			} catch(Exception e) {
				fail("Get new position failed.");
			}
			// energy shouldn't change
			assertEquals(oldEnergy, r.getBatteryLevel());
			// face shouldn't change
			assertEquals(oldFace, r.getCurrentDirection());
		}
	}
	
	static Stream<Arguments> allCombinationsManualAndDirections() {
		ArrayList<Arguments> allCombos = new ArrayList<Arguments>();
		for(Turn turn: Turn.values()) {
			for (final boolean manual : new boolean[] { false, true} ) {
				allCombos.add(Arguments.of(manual, turn));
			}

		}
	    return allCombos.stream();
	}
}
