package gui;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

class WizardTest extends Wizard {

	public boolean drive2Exit(float batteryLevel) throws Exception {
		this.robot.setBatteryLevel(batteryLevel);
		return this.drive2Exit();
	}
	FakeMazeApplication fma;

	@AfterEach
	public void tearDown() {
		fma.dispose();
	}
	
	/**
	 * Test if Wizard can drive to exit on a variety of mazes.
	 */
	@Test
	void testDriveToExit() {
		TestConfig t = TestConfig.getTestConfig("TwoDoorsSameCell");
		t.setDriver(this);
		
		fma = new FakeMazeApplication(t);
		fma.start(null);
		try {
			this.drive2Exit(10000);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertTrue(this.robot.isAtExit());
	}
	

}
