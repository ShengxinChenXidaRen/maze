package gui;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

class WallFollowerTest extends WallFollower{
	FakeMazeApplication fma;
	
	@AfterEach
	public void tearDown() {
		fma.dispose();
	}
	
	/**
	 * Test WallFollower in a small perfect maze
	 */
	@Test
	void testDriveToExitEasy() {
		TestConfig t = TestConfig.getTestConfig("SmallPerfectMaze");
		t.setDriver(this);
		
		fma = new FakeMazeApplication(t);
		fma.start(null);
		try {
			this.drive2Exit();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertTrue(this.robot.isAtExit());
	}
	
	/**
	 * Test WallFollower in a large perfect maze 
	 */
	@Test
	void testDriveToExitHard() {
		TestConfig t = TestConfig.getTestConfig("BigPerfectMaze");
		t.setDriver(this);
		fma = new FakeMazeApplication(t);
		fma.start(null);
		try {
			this.drive2Exit();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertTrue(this.robot.isAtExit());
	}
		
	@Test
	void testInsideRoom() {
		TestConfig t = TestConfig.getTestConfig("PlacedInRoom");
		t.setDriver(this);
		fma = new FakeMazeApplication(t);
		fma.start(null);
		try {
			this.drive2Exit();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertTrue(this.robot.isAtExit());
	}
}
