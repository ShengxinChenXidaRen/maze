/**
 * 
 */
package gui;

/**
 * This class is a wrapper class to startup the Maze game as a Java application
 * except, it uses the dummy controller.
 * 
 * This code is copied from MazeApplication, with some changes
 */
public class FakeMazeApplication extends MazeApplication {

	private TestConfig testSetup;
	public Controller controller;
	// not used, just to make the compiler, static code checker happy
	private static final long serialVersionUID = 1L;

	public FakeMazeApplication() {
		throw new RuntimeException("No");
	}

	/**
	 * Constructor for testing
	 * 
	 * @param seed the seed
	 * @param difficulty the difficulty
	 * @param startingPosition the starting position
	 * @param isPerfect whether the maze is perfect
	 * @param builder the builder used to generate the maze
	 * @param driver driver object, usually the one you want to test
	 * @param robot robot object, usually the one you want to test
	 */
	public FakeMazeApplication(TestConfig testSetup) {
		this.testSetup = testSetup;
	}

	@Override
	/**
	 * Instantiates a controller with settings according to the given parameter.
	 * @param parameter can identify a generation method (Prim, Kruskal, Eller)
	 * or a filename that contains a generated maze that is then loaded,
	 * or can be null
	 * @return the newly instantiated and configured controller
	 */
	 Controller createController(String parameter) {
		controller = new DummyController(testSetup);
		controller.setBuilder(testSetup.getBuilder());
	    dbg("Created and returning controller.");
	    return controller;
	}
	
	/**
	 * Main method to launch Maze game as a java application.
	 * 
	 * Set you maze here.
	 */
	public static void main(String[] args) {
		TestConfig t = TestConfig.getTestConfig("TestMaze1");
		t.setDriver(new ManualDriver());
		FakeMazeApplication fma = new FakeMazeApplication(t);
		fma.start(null);
	}
	
	private void dbg(Object o) {
		System.out.println("[FakeMazeApplication]" + o.toString());
	}

}
