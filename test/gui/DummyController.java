package gui;

import java.util.concurrent.CountDownLatch;

import generation.BSPBuilder;
import generation.CardinalDirection;
import generation.Cells;
import generation.Distance;
import generation.MazeConfiguration;
import generation.MazeContainer;
import generation.Order;
import generation.StubOrder;
import generation.Wall;
import gui.TestConfig.TestMaze;

public class DummyController extends Controller {
	MazeConfiguration maze;
	StatePlaying sp;

	
	private TestConfig testSetup;
	
	final int HEIGHT = 4;
	final int WIDTH = 4;
	
    private final CountDownLatch generationLatch = new CountDownLatch (1);

	
	/**
	 * This constructor generates a maze to test with.
	 * @param seed
	 * @param difficulty
	 * @param startingPosition
	 * @param driver
	 * @param robot
	 */
	public DummyController(TestConfig testSetup) {
		super();
		super.setDriverType(testSetup.driver.driverType);
		this.testSetup = testSetup;
		dbg("Created.");
	}
	@Override
	public void start() {
		super.start();
		super.setBuilder(testSetup.getBuilder());
		super.setDriverType(testSetup.driver.driverType);
		super.setPerfect(testSetup.isPerfect());
		super.setSeed(testSetup.getSeed());
		if(testSetup.tm == TestMaze.TestMaze1) {
			switchFromGeneratingToPlaying(makeTestMaze1());
			return;
		}
		dbg("Switching from title to generating.");
		super.switchFromTitleToGenerating(testSetup.getDifficulty());
		try {
			generationLatch.await();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void startWithMaze(MazeConfiguration maze) {
		super.start();
		super.setBuilder(null);
		super.setDriverType(null);
		super.setPerfect(false);
		super.setSeed(null);
		switchFromGeneratingToPlaying(maze);
	}
	
	@Override
	public void switchFromGeneratingToPlaying(MazeConfiguration config) {
        currentState = states[2];
        currentState.setMazeConfiguration(config);
	    // set driver & robot
        super.setRobotAndDriver(testSetup.robot, testSetup.driver);
        this.robot.setMaze(this);
        this.driver.setRobot(this.robot);
        this.driver.setDimensions(config.getWidth(), config.getHeight());
        this.driver.setDistance(config.getMazedists());
		StatePlaying sp = (StatePlaying)super.currentState;
        currentState.start(this, panel);
		sp.setCurrentPosition(testSetup.startingPosition[0], testSetup.startingPosition[1]);
		sp.draw(); // redraw to reflect new starting position
		generationLatch.countDown();
	}

	/**
	 * Create a test maze
	 * @return a silly little test-maze
	 */
	private MazeContainer makeTestMaze1() {
		MazeContainer maze = new MazeContainer();
		maze.setHeight(HEIGHT);
		maze.setWidth(WIDTH);
		Cells cells = makeCells1();
		maze.setMazecells(cells);
		Distance dists = new Distance(WIDTH, HEIGHT);
		maze.setMazedists(dists);
		final int colchange = 0;
		final Order dummyOrder = new StubOrder(0, null, false);
		final BSPBuilder b = new BSPBuilder(dummyOrder, dists,
				cells, WIDTH, HEIGHT, colchange, 60) ;
		try {
			maze.setRootnode(b.generateBSPNodes());
		} catch (InterruptedException e) {
			e.printStackTrace();
		} 
		cells.setInRoomToOne(0, 3);
		maze.setStartingPosition(2, 1);
		assert maze.getHeight() == 4;
		return maze;
	}
	/**
	 * Initialize 3by3 maze with map looking like this:
	 * 
	 * +---+---+---+---+
	 * |0,3 1,3 2,3    
	 * +   +---+   +   +
	 * |       |       |
	 * +   +---+   +
	 * |ROM 1,1 2,1    |
	 * +   +   +   + 
	 * |0,0 1,0 2,0    |
	 * +---+---+---+---+
	 */
	private Cells makeCells1() {
		Cells cells = new Cells(WIDTH, HEIGHT);
		cells.initialize();
		// break all inner walls
		// horizontal
		for(int x =0; x < WIDTH; x++) {
			for(int y=0; y < HEIGHT-1; y++){
				Wall southWall = new Wall(x, y, CardinalDirection.South);
				cells.deleteWall(southWall);
			}
		}
		for(int x=0; x < WIDTH-1; x++) {
			for(int y=0; y < HEIGHT; y++) {
				Wall eastWall = new Wall(x, y, CardinalDirection.East);
				cells.deleteWall(eastWall);
			}
		}
		Wall[] walls = {
				new Wall(1,2, CardinalDirection.North),
				new Wall(1,2, CardinalDirection.South),
				new Wall(1,2, CardinalDirection.East),
		};
		for (Wall wall : walls) {
			cells.addWall(wall, true);
		}
		cells.setExitPosition(3, 3);
		return cells;
	}
	
	private void dbg(Object o) {
		System.out.println(o.toString());
	}
}
