/**
 * 
 */
package gui;

import generation.CardinalDirection;
import generation.Order.Builder;

/**
 * @author xren
 *
 */
public class TestConfig {
	public enum TestMaze{
		PlacedInRoom(){
			void init(){
				builder = Builder.Eller;
				difficulty = 1;
				perfect = false;
				seed = 3;
			}
		},
		TwoDoorsSameCell(){
			void init() {
				builder = Builder.Eller;
				difficulty = 5;
				perfect = false;
				seed = 3;
			}
		},
		BigPerfectMaze(){
			void init() {
				builder = Builder.Eller;
				difficulty = 10;
				perfect = true;
			}
		},
		SmallPerfectMaze(){
			void init() {
				builder = Builder.Eller;
				difficulty = 2;
				perfect = true;
			}
		},
		TestMaze1(){
			void init() {
				builder = Builder.DFS;
				difficulty = 0;
				perfect = false;
			}
		}
		;
		
		public Builder builder;
		public int difficulty;
		public boolean perfect;
		public int seed;
		
		abstract void init();
		
		private TestMaze() {
			init();
		}
	}
	
	static public TestConfig getTestConfig(String descriptor) {
		TestConfig t = new TestConfig();
		switch(descriptor) {
		case "PlacedInRoom":
			t.tm = TestMaze.PlacedInRoom;
			t.setStartingPosition(new int[] {3,4});
			t.setInitialFace(CardinalDirection.East);
			t.setAllottedEnergy(10000);
			break;
		case "TwoDoorsSameCell":
			t.tm = TestMaze.TwoDoorsSameCell;
			t.setStartingPosition(new int[] {20,6});
			t.setInitialFace(CardinalDirection.North); // north is down
			t.setAllottedEnergy(10000);
			break;
		case "SmallPerfectMaze":
			t.tm = TestMaze.SmallPerfectMaze;
			t.setStartingPosition(new int[] {0,0});
			break;
		case "BigPerfectMaze":
			t.tm = TestMaze.BigPerfectMaze;
			t.setStartingPosition(new int[] {0,0});
			break;
		case "TestMaze1":
			t.tm = TestMaze.TestMaze1;
			t.setStartingPosition(new int[] {1,2});
			break;
		default:
			throw new RuntimeException(String.format("No TestConfig corresponding to you string %s", descriptor));
		}
		// default robots;
		t.setDriver(new ManualDriver());
		t.setRobot(new BasicRobot());
		return t;
	}
	
	TestMaze tm;
	public float allottedEnergy;
	
	/**
	 * @return the builder
	 */
	public Builder getBuilder() {
		return tm.builder;
	}
	/**
	 * @return the difficulty
	 */
	public int getDifficulty() {
		return tm.difficulty;
	}
	/**
	 * @return the isPerfect
	 */
	public boolean isPerfect() {
		return tm.perfect;
	}
	/**
	 * @return the seed
	 */
	public Integer getSeed() {
		return tm.seed;
	}

	public Driver driver;
	public CardinalDirection initialFace = CardinalDirection.East; // default face
	public Robot robot;
	public int[] startingPosition;


	/**
	 * @param allottedEnergy the allottedEnergy to set
	 */
	public void setAllottedEnergy(float allottedEnergy) {
		this.allottedEnergy = allottedEnergy;
	}
	/**
	 * @param driver the driver to set
	 */
	public void setDriver(Driver driver) {
		this.driver = driver;
	}
	/**
	 * @param initialFace the initialFace to set
	 */
	public void setInitialFace(CardinalDirection initialFace) {
		this.initialFace = initialFace;
	}
	/**
	 * @param robot the robot to set
	 */
	public void setRobot(Robot robot) {
		this.robot = robot;
	}
	/**
	 * @param startingPosition the startingPosition to set
	 */
	public void setStartingPosition(int[] startingPosition) {
		this.startingPosition = startingPosition;
	}
}
