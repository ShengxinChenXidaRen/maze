package gui;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

class PledgeTest extends Pledge{
	FakeMazeApplication fma;
	@AfterEach
	public void tearDown() {
		fma.dispose();
	}
	
	
	@Test
	void testDriveToExit() {
		TestConfig t = TestConfig.getTestConfig("TwoDoorsSameCell");
		t.setDriver(this);
		t.setRobot(new BasicRobot());
		t.setStartingPosition(new int[] {0,0});
		
		fma = new FakeMazeApplication(t);
		fma.start(null);
		try {
			this.drive2Exit();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertTrue(this.robot.isAtExit());
	}
	
	@Test
	void testInsideRoom() {
		TestConfig t = TestConfig.getTestConfig("PlacedInRoom");
		t.setDriver(this);
		t.setRobot(new BasicRobot());
		
		fma = new FakeMazeApplication(t);
		fma.start(null);
		try {
			this.drive2Exit();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertTrue(this.robot.isAtExit());
	}

}
