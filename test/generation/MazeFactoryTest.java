package generation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Collection;
import java.util.Queue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import generation.Order.Builder;

/**
 * Test integrety of generated maze.
 * @author xren
 * <p>
 * Tests depend on whether the maze is perfect.
 * 
 * Tests are parametric.
 * 
 * @see <a href="https://www.guru99.com/junit-parameterized-test.html"> Parameterized testing </a>
 */
@RunWith(Parameterized.class)
public class MazeFactoryTest {
	private MazeConfiguration maze;
	
	int skillLevel;
	private boolean perfect;
	Builder builder;
	
	/**
	 * Constructs maze test; reads in parameters using junit parametric testing.
	 * <p>
	 * 
	 * </p>
	 * @param skillLevel: 0-9, determines the size of the maze
	 * @param perfect: if true, maze paths is a spanning tree of maze cells;
	 *                 this means that the maze has no loops and no isolated cells.
	 * @param builder: determines algorithm for maze construction.
	 */
	public MazeFactoryTest(int skillLevel, boolean perfect, Builder builder) {
		this.skillLevel = skillLevel;
		this.perfect = perfect;
		this.builder = builder;
	}
	
	/**
	 * Generate a list of parameters to test on.
	 * @return array of parameter sets. each parameter set is an array: {skillLevel, perfect, builder}
	 * @see <a href="https://stackoverflow.com/questions/650894/changing-names-of-parameterized-tests">name</a>
	 */
	@Parameterized.Parameters(name="{index}: MazeFactoryTest(skillLevel={0}, perfect={1}, builder={2}")
	public static Collection<Object[]> getParameters() {
		Object[][] parameters = new Object[][] {
			{0, true, Builder.DFS},
			{0, true, Builder.Eller},
			{3, true, Builder.Eller},
			{5, true, Builder.Eller},
			{3, false, Builder.Eller},
			{5, false, Builder.Eller},
		};
		return Arrays.asList(parameters);
	}
	
	
	
	@Before
	public void setUp() throws Exception {
		StubOrder order = new StubOrder(skillLevel, builder, perfect);
		order.setSeed(42);
		
		Factory factory = new MazeFactory(); // determinstic: true
		
		boolean order_successful = factory.order(order); // begin maze production
		System.out.println("order success: " + order_successful);
		
		factory.waitTillDelivered();
		this.maze = order.getDelivery();
		System.out.println("Percentage done: " + order.getPercentDone());
	}

	@Test
	public final void testMazeReady() {
		assertNotNull(maze);
		// make sure maze is ready
		assertNotNull(maze.getWidth());
		System.out.println(maze.getWidth());
		assertNotNull(maze.getHeight());
		System.out.println(maze.getHeight());
	}
	
	/**
	 * Test some properties related to the exit.
	 * Does not assume perfect maze properties.
	 */
	@Test
	public final void testExit() {		
		int nExits = 0;

		Cells cells = maze.getMazecells();
		
		for (int x=0; x < maze.getWidth(); x++){
			for(int y=0; y < maze.getHeight(); y++){
				int thisDist = maze.getDistanceToExit(x, y);
				
				assertTrue(
						"Every point in the maze must be at least 1 from exit",
						thisDist >= 1);
				if(perfect) {
					assertTrue(
							"Every point in the maze must be no further than h*w from exit",
							thisDist <= maze.getHeight() * maze.getWidth());
				}
				assertEquals(
						"A cell is an exit position if and only if its distance to the exit is 1",
						thisDist == 1, cells.isExitPosition(x, y));
				
				if(thisDist == 1) {
					nExits += 1;
				}
			}
		}
		assertTrue(
				"The maze must have exactly one exit.",
				nExits == 1);
	}
	
	
	/**
	 * Make sure that the starting position exists,
	 * and is the farthest from the exit.
	 */
	@Test
	public final void testStartingPosition() {
		int[] startingPosition = maze.getStartingPosition();
		
		int startingDist = maze.getDistanceToExit(
				startingPosition[0],
				startingPosition[1]);
		
		for (int x=0; x < maze.getWidth(); x++){
			for(int y=0; y < maze.getHeight(); y++){
				int thisDist = maze.getDistanceToExit(x, y);
				assertTrue(
						"No point can be further to exit than starting point",
						thisDist <= startingDist);
			}
		}
	}
	
	/**
	 * When perfect is specified, the generated maze should be perfect;
	 * i.e. its pathways should be a spanning tree, with no cycles
	 */
	@Test
	public final void testPerfectMaze() {
		if(!perfect) {
			return;
		}
		
		Cells cells = maze.getMazecells();

		int[][] visited = new int[maze.getWidth()][maze.getHeight()];
		
		int n_visited = 0;
		
		Queue<Wall> q = new ArrayDeque<Wall>();
		
		// visit first cell
		visited[0][0] = 1;
		if(!cells.hasWall( 0, 0, CardinalDirection.South))
			q.add(new Wall(0, 0, CardinalDirection.South));
		if(!cells.hasWall( 0, 0, CardinalDirection.East))
			q.add(new Wall(0, 0, CardinalDirection.East));
		n_visited += 1;
		
		
		while(!q.isEmpty()) {
			Wall cur = q.remove();
			
			int x = cur.getNeighborX();
			int y = cur.getNeighborY();
			
			if(x<0 || x >= maze.getWidth() || y<0 || y >= maze.getHeight()) {
				continue;
			}
			
			n_visited += 1;
			assertEquals(
					"In a perfect maze, each cell should only be reached once by BFS",
					0, visited[x][y]);
			
			visited[x][y] += 1;
			
			CardinalDirection cd = cur.getDirection().oppositeDirection();
			for(int i = 0; i < 3; i++) {
				cd = cd.rotateClockwise();
				if(cells.hasWall(x, y, cd)) {
					continue; // skip unreachable
				}
				Wall to_explore = new Wall(x,y,cd);
				q.add(to_explore);
				
			}
		}
		
		int n_cells = maze.getWidth() * maze.getHeight();
		System.out.println("n_cells: " + n_cells + ", n_visited: " + n_visited);
		assertTrue(
				"All cells should be visited by the end of the BFS",
				n_cells <= n_visited);
	}
}
