package generation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import generation.Order.Builder;
import gui.Constants;

public class MazeBuilderEllerTest extends MazeBuilderEller {
	@Before
	public void setUp() {
		this.order = new StubOrder(3, Builder.Eller, true);
		int skill = order.getSkillLevel() ;
		// derive parameters
		width = Constants.SKILL_X[skill];
		height = Constants.SKILL_Y[skill];// rooms can result in loops, so for a perfect maze, set room number to 0
		// instantiate data structures
		cells = new Cells(width,height) ;
		dists = new Distance(width,height) ;
	}
	
	@Test
	public void testBlockingRoom() {
		// enclose a box blocking the progression of the algorithm
		int rw = width;
		int rh = 1;
		int rx = 0;
		int ry = 5;
		int rxl= rx + rw - 1;
		int ryl= ry + rh - 1;
		super.cells.encloseArea(rx, ry, rxl, ryl);
		
		super.generatePathways();
		// test if upper walls of the "room" have been broken
		int n_upper_walls = 0;
		for(int x = 0; x < rw; x++) {
			if(super.cells.hasWall(x, ry, CardinalDirection.North)) {
				n_upper_walls += 1;
			}
		}
		assertTrue(
				"Some upper walls should have been broken.",
				n_upper_walls < rw);
		// test if lower walls of the "room" have been broken
		int n_lower_walls = 0;
		for(int x = 0; x < rw; x++) {
			if(cells.hasWall(x, ry, CardinalDirection.South)) {
				n_lower_walls += 1;
			}
		}
		assertTrue(
				"Some lower walls should have been broken.",
				n_lower_walls < rw);
	}

	@Test
	public void testLastRowAllBreak() {
		cells.initialize();
		int[] lastRowBefore = new int[width];
		for (int i = 0; i < lastRowBefore.length; i++){
			lastRowBefore[i] = i;
		}
		super.breakRightWalls(lastRowBefore, height-1);
		
		int n_walls = 0;
		for (int x = 0; x < width -1; x ++) {
			assertFalse("All sets in the last row should be joined.", cells.hasWall(x, height - 1, CardinalDirection.East));
			n_walls += cells.hasWall(x, height - 1, CardinalDirection.East)?1:0;
		}
		System.out.println("Walls: " + n_walls + ", width: " + width);
		
		assertEquals(0, n_walls);
	}
	
	@Test
	public void testLastRowNoAllBreak() {
		cells.initialize();
		int[] lastRowBefore = new int[width];
		for (int i = 0; i < lastRowBefore.length; i++){
			lastRowBefore[i] = 1;
		}
		//super.breakRightWalls(lastRowBefore, height-1);
		
		int n_walls = 0;
		for (int x = 0; x < width -1; x ++) {
			n_walls += cells.hasWall(x, height - 1, CardinalDirection.East)?1:0;
		}
		System.out.println("Walls: " + n_walls + ", width: " + width);
		assertEquals(width - 1, n_walls);
	}
	

}
