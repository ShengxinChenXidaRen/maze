/**
 * 
 */
package generation;

import java.util.concurrent.TimeoutException;



/**
 * @author xren
 * This is a stub order class used to inspect factories.
 * Submit it to a factory, and 
 */
public class StubOrder implements Order {
	// privates
	private int skillLevel; // user selected skill level, i.e. size of maze
    private Builder builder; // selected maze generation algorithm
    private Integer seed;
    
    /**
	 * @return the seed
	 */
	public Integer getSeed() {
		return seed;
	}

	/**
	 * @param seed the seed to set
	 */
	public void setSeed(Integer seed) {
		this.seed = seed;
	}
	private boolean perfect; // selected type of maze, i.e. 
    // perfect == true: no loops, i.e. no rooms
    // perfect == false: maze can support rooms
    
    private int percentdone;        // describes progress during generation phase
    
    private MazeConfiguration deliverable;
    
	
	/**
	 * 
	 */
	public StubOrder(int skillLevel, Builder builder, boolean perfect) {
		// take in publics
	    this.skillLevel = skillLevel;
	    this.builder = builder;
	    this.perfect = perfect;
	    // init private variables
	    this.percentdone = 0;	    
    }
	
	/**
	 * 
	 */
	public MazeConfiguration getDelivery() throws TimeoutException {
		
		return this.deliverable;
	}


    /**
     * The deliver method is the call back method for the background
     * thread operated in the maze factory to deliver the ordered
     * product, here the generated maze in its container, 
     * the MazeConfiguration object.
     */
    @Override
    public void deliver(MazeConfiguration mazeConfig) {
    	this.deliverable = mazeConfig;
    }
    
    //////////// set of trivial get methods ////////////////////////
    @Override
    public int getSkillLevel() {
        return skillLevel;
    }
    @Override
    public Builder getBuilder() {
        return builder;
    }
    @Override
    public boolean isPerfect() {
        return perfect;
    }
    
    public int getPercentDone() {
        return percentdone;
    }
    /**
     * Allows external increase to percentage in generating mode.
     * Internal value is only updated if it exceeds the last value and is less or equal 100
     * @param percentage gives the new percentage on a range [0,100]
     * @return true if percentage was updated, false otherwise
     */
    @Override
    public void updateProgress(int percentage) {
        if (this.percentdone < percentage && percentage <= 100) {
            this.percentdone = percentage;
        }
    }
}
